let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
    penulis: "John Doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemrograman dasar",
    jumlahHalaman: 172,
    warnaSampul: [warna, "hitam"]
}

let updateBuku = {
    ...buku,
    ...dataBukuTambahan
}

console.log(updateBuku);